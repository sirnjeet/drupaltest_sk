<?php
/**
 * @file
 * kaplan_content_type.features.inc
 */

/**
 * Implements hook_node_info().
 */
function kaplan_content_type_node_info() {
  $items = array(
    'kaplan' => array(
      'name' => t('Kaplan'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
