<?php

/**
 * @file
 * Config form for qTip2.
 */

/**
 * Form builder function for module settings.
 */
function _qtip2_config_form( $form_state )
{
	$node_types = node_type_get_types();
	$node_options = array();
	foreach( $node_types as $node_type )
	{
		$node_options[ $node_type->type ] = $node_type->name;
	}
		
	$form['qtip2_node'] = array(
		'#type'				=> 'select',
		'#options'			=> $node_options,
		'#title'			=> t('Content type'),
		'#default_value'	=> variable_get('qtip2_node', 'kaplan'),
		'#description'		=> t('Content type to show tooltips on (machine name).'),
		'#required'			=> TRUE,
	);


	$form['qtip2_target'] = array(
		'#type'				=> 'textfield',
		'#title'			=> t('Target field'),
		'#default_value'	=> variable_get('qtip2_target', 'body'),
		'#description'		=> t('The field to target for the tooltip (machine name). Hovering over this field will display the tooltip.'),
		'#size'				=> '60',
		'#required'			=> TRUE,
	);

	$form['qtip2_msgfield'] = array(
		'#type'				=> 'textfield',
		'#title'			=> t('Tooltip field'),
		'#default_value'	=> variable_get('qtip2_msgfield', 'field_message'),
		'#description'		=> t('The field that contains the tooltip message (machine name).'),
		'#size'				=> '60',
		'#required'			=> TRUE,
	);
	
	//We could add more here such as add paramters that could be passed to qTip2 to alter the display etc
	
	//Form submit processing
	$form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

	return $form;

}


/**
 * Processes _qtip2_config_form when submitted
 *
 */
function _qtip2_config_form_submit($form, &$form_state)
{
	//Get the form fields
	$form_fields = $form_state['values'];

	//Save the form values to our module variables
	variable_set( 'qtip2_node', trim( $form_fields['qtip2_node'] ));
	variable_set( 'qtip2_target', trim( $form_fields['qtip2_target'] ) );
	variable_set( 'qtip2_msgfield', trim( $form_fields['qtip2_msgfield'] ) );

	drupal_set_message( t( 'Your settings have been saved.' ) );
}

?>