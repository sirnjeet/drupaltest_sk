/* Add the qTip on the page if appropriate */
(function ($) {
	Drupal.behaviors.qtip2 = {
		attach: function (context, settings) {

			$(function() {
				if( Drupal.settings.qtip2 )
				{
					
					$(document).ready(
					
						function() {
							$( Drupal.settings.qtip2.qtip2_targ ).qtip({content: { text: $( Drupal.settings.qtip2.qtip2_msg ) } });
						}
						
					);
				}
			});
		}
	};
})(jQuery);

